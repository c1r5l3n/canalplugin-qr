<?php

use Flood\Canal\AppService\QrGeneratorLibrary;

return static function($data) {
    if(!filter_has_var(INPUT_POST, 'submit')) {
        error_log('QR-Request was not submitted!!');
    } else {
        error_log('Yayyyy! Post submitted!');

        //This is qr-Array with LinkArray;
        /*$linkArray = QrGeneratorDataHandling::getArrayFromDataString(); */
        $linkArray = preg_split("/[\s,]+/", $_POST['links']);



        //saves QRs as SVG
        QrGeneratorLibrary:: renderQr($linkArray);
        $svgArray = QrGeneratorLibrary::getFilePathArray($linkArray);



        $format = $_POST['dataformat'];
        $color = $_POST['color'];
        $resolution = $_POST['resolution_dpi'];
        $size = $_POST['size'];

        //if inputfields are not filled, keep imageparameters of former svg-file
        if(!filter_has_var(INPUT_POST, 'dataformat')) {
            $format = 'svg';
        }
        if(!filter_has_var(INPUT_POST, 'color')) {
            $color = 'black';
        }
        if(!filter_has_var(INPUT_POST, 'size')) {
            $size = 200;
        }
        //change format and parameters
        QrGeneratorLibrary::changeFormatAndParametersOfQrFiles($linkArray, $format, $size);

        //gets array of svg-files


        $data['qrArray'] = $svgArray;
    }

    return $data;
};