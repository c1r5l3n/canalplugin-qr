# QR-generator plugin for flood/canal

## Usage

This block ist for a Flood\Canal project.
You can create one or multiple QR Codes from your data: Add the block to a canal-page, insert one or multiple links in the HTML-Form from the template, select your preferred format, size and color of QRs. The rendered qr-codes are saved to root/data/content/out/qr. By default, the format is svg, the size 200:200 and color black.

## How to integrate it to your canal-project

To add a qr-generator, you have to add it to `_content.php`

Add the following code to the function:

    $content->addBlock('QR', __DIR__ . /vendor/canalplugin/qr/src');
    
For saving the qrs, add the following directory to your project: `(root)/data/content/out/qrcodes`

No further config or setup required.
          
## Licence

This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more informations on the Licences which are applied read: [LICENCE.md](LICENCE.md)

## Copyright

Carolin Holat

#### Thanks to bacon/bacon-qr-code, which is used as basis

